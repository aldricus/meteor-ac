Package.describe({
  name: 'aldricus:autocomplete',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'Simple autoload widget',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/aldricus/meteor-ac',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');
  api.addFiles('client/autocomplete.js', 'client');
  api.addFiles('client/autocomplete.css', 'client');
  api.addAssets('client/loader.gif', 'client');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('aldricus:autocomplete');
  api.addFiles('tests/aldricus:autocomplete-tests.js');
});
