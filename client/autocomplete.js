/*
    Meteor Autocomplete - Copyright (C) 2015 Aldricus
    aldric.dev@gmail.com

 	License:  GNU General Public License

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
(function(){
	'use strict';
	var AutoComplete = function(selector){

		var KEYS = {UP: 38, DOWN: 40, ENTER: 13, ESCAPE: 27, TAB: 9};

		var $this; // Autocomplete instance
		var ac = []; // autocomplete namespace // NE SERT A RIEN
		var searchTerm;

		ac[selector] = {};
		ac[selector].$container = $(selector);
		var inputAC = ac[selector].$container.find('input, textarea');
		if(ac[selector].$container[0].tagName == 'INPUT'){
			ac[selector].$input = ac[selector].$container;
		}else if(ac[selector].$container[0].tagName == 'TEXTAREA'){
			ac[selector].$input = ac[selector].$container;
		}else if(inputAC.length > 0){
			ac[selector].$input = inputAC;
		}else{
			throw Error('not input element found');
		}

		ac[selector].$input.attr('autocomplete', 'off');
		ac[selector].$input.css('lineHeight' ,'0px');

		var list = {
			listItems: {},
			selectedValue: 0,
			selector: selector,


			init: function(options){
				$this = this;

				/**
				 *
				 * options : {
				 *
				 * 		source: function(searchTerm, responseCallback){
				 *			var data = ['Paris', 'Casablanca', 'Merzouga', 'Toulouse'];
				 *			responseCallback(data);
				 *		},
				 *		template: function(data,i){
				 *			return '<div class="box"><span>'+data[i]+'</span></div>';
				 *		},
				 *		templateName: <template name>,
				 *		clickItem: function(el, item){
				 *			el.value = item.getElementsByTagName('span')[0].innerHTML;
				 *		},
				 *		onLoaded: function(){},
				 *		termLength: 1 // default;
				 * }
				 */
				var hideList = function(){
					$this.selectedValue = 0;
				 	ac[selector].cancel = false;

				 	setTimeout(function(){
				 		if(!ac[selector].cancel){
				 			ac[selector].$ul.hide();
				 		}
				 	}, 500);

				 };
				 ac[selector].options = options;
				 ac[selector].cancel = true;
				 var parent = ac[selector].$container.parent();
				 var uniqueId = _.uniqueId('ldrc-ac-');
				 parent.append('<ul class="' + uniqueId +'"></ul>');
				 ac[selector].$ul = $('.' + uniqueId);
				//placement de la liste pour afficher
				//le résultat
				var cs = getInputWidth(ac[selector].$input);

				ac[selector].$ul.css('display','none');
				if(cs.w > 200){
					ac[selector].$ul.css('maxWidth',cs.w + 'px');
				}else{
					ac[selector].$ul.css('maxWidth','300px'); // Default autocomplete width
				}
				ac[selector].$ul.css('minWidth',cs.w + 'px');
				ac[selector].$ul.addClass('ldrc-autocomplete');

				// Attach some events handler
				ac[selector].$input.on('click', getSearchTerm);
				ac[selector].$input.on('keyup', getSearchTerm);
				ac[selector].$input.on('mouseleave, blur', hideList);

				ac[selector].$ul.on('mouseenter',function(){
				 	ac[selector].cancel = true;
				});
			}

		};

		/**
		 * Méthode qui formatte et affiche la
		 * liste avec les valeurs recherchées
		 */
		 var responseCallback = function(data){
		 	// Hide any already openned autocomplete widgets
			$('html').find('.ldrc-autocomplete').hide();

		 	$this.selectedValue = 0;
		 	var innerHtml = '';
		 	var re = new RegExp(searchTerm, 'i');
		 	var l = data.length;
		 	for (var i = 0; i  < l; i++) {
		 		if(ac[$this.selector].options.template === undefined){
		 			if(searchTerm.length > 1){
		 				if(re.test(data[i])){
							//affiche seulement les valeurs
							//qui correspondent au RegExp
							innerHtml += '<li data-idx="i'+ i +'">' + data[i] + '</li>';
						}
					}else if(searchTerm.length == 1){
						//affiche toute les valeurs
						//TODO: a modifier
						innerHtml += '<li data-idx="i'+ i +'">' + data[i] + '</li>';
					}
				}else{
					if(searchTerm.length > 1){
						//affiche seulement les valeurs
						//qui correspondent au RegExp avec les templates
						//paramétré dans le callback
						innerHtml += '<li data-idx="i'+ i +'">' + ac[$this.selector].options.template(data, i)  + '</li>';
					}else if(searchTerm.length == 1){
						//affiche toute les valeurs
						innerHtml += '<li data-idx="i'+ i +'">' + ac[$this.selector].options.template(data, i)  + '</li>';
					}
				}
			}
			if(innerHtml !== ''){
				// Display current autocomplete
				ac[$this.selector].$ul.show();
				ac[$this.selector].$ul.html(innerHtml);
			}else{
				ac[$this.selector].$ul.hide();
			}

			$this.listItems = ac[$this.selector].$ul.find('li');

			if($this.listItems.length > 0){
				$this.listItems.on('click', function(){
					getItem(this);
				});
				$this.listItems.on('mouseenter', function(e){
					ac[$this.selector].$ul.find('.ldrc-state-focus').removeClass('ldrc-state-focus');
					$(this).addClass('ldrc-state-focus');
					$this.$selectedItem=  $(this);
					$this.selectedValue = parseInt($(this).attr('data-idx').substring(1));

				});
			}
			//rapatrié de getSearchTerm
			var item;
			if((item = $this.listItems[$this.selectedValue]) !== undefined){
				//item.className = 'ldrc-state-focus';
				$(item).addClass('ldrc-state-focus');
				$this.$selectedItem = $(item);
			}
		};

		/**
		 * Méthode pour récupérer le terme à rechercher.
		 */
		 var getSearchTerm = function(e){
		 	e.preventDefault();
		 	searchTerm = ac[selector].$input.val();
		 	var termLength = 1; // Default term string length to trigger the search event
		 	if(ac[$this.selector].options.termLength){
		 		termLength = ac[$this.selector].options.termLength;
		 	}
		 	if(searchTerm.length >= termLength){
		 		setOffsetPosition(); // set autocomplete position
			 	ac[$this.selector].$ul.find('.ldrc-state-focus').removeClass('ldrc-state-focus');

			 	var l = 0;
			 	if($this.listItems !== undefined){
			 		l = $this.listItems.length;
			 	}

			 	e = e || window.event;

			 	if (e.keyCode == KEYS.DOWN && $this.selectedValue < l - 1){
			 		$this.listItems.removeClass('ldrc-state-focus');
					$this.listItems[++$this.selectedValue].className = 'ldrc-state-focus';
					$this.$selectedItem = $($this.listItems[$this.selectedValue]);
				}else if (e.keyCode == KEYS.UP && $this.selectedValue > 0) {
					$this.listItems.removeClass('ldrc-state-focus');
					$this.listItems[--$this.selectedValue].className = 'ldrc-state-focus';
					$this.$selectedItem = $($this.listItems[$this.selectedValue]);

				}else if (e.keyCode == KEYS.ENTER) {
					getItem($this.$selectedItem[0]);
					$this.selectedValue = 0;
				}else if(e.keyCode != KEYS.UP && e.keyCode != KEYS.DOWN && e.keyCode != KEYS.ESCAPE && e.keyCode != KEYS.ENTER) {

				 	ac[selector].$ul.html('<li><div class="loader"></div></li>');
					ac[selector].$ul.show();
					if($this.selectedValue < l - 1 || $this.selectedValue === 0){
						ac[$this.selector].options.source(searchTerm, responseCallback);
					}
				}else if(e.keyCode == KEYS.ESCAPE){
					ac[$this.selector].$ul.hide();
					$this.selectedValue = 0;
					ac[$this.selector].$input.blur();
				}else{
					$this.listItems[$this.selectedValue].classList.add('ldrc-state-focus');
				}
			}else{
				ac[selector].$ul.empty();
			}
		};

		/**
		* On récupère la postition de l'élément champ
		* de saisi pour positionner la liste d'autocomplétion
		*/
		var getInputWidth = function($el){
			var w = $el.outerWidth();

			var h = $el.height();

			return {h:h, w:w};
		};
		var setOffsetPosition = function(){
			var posInput =  ac[$this.selector].$input.offset();
			var top = ac[$this.selector].$input.height() + posInput.top;
			var left = posInput.left;
			console.log($this.selector, ac[$this.selector].$input.offset());
			ac[$this.selector].$ul.css('left',left +'px');
			ac[$this.selector].$ul.css('top', top+'px');
			// OnLoaded callback
			if(ac[$this.selector].options.onLoaded){
				ac[$this.selector].options.onLoaded();
			}
		};

		var getItem = function(item){

			if(ac[$this.selector].options.clickItem === undefined){
				//comportement par défaut: on récupère tout le contenu du liste item
				ac[$this.selector].$input.html(item.innerHTML);
			}else{
				//comportement défini par la fonction de configuration clickItem
				ac[$this.selector].options.clickItem(ac[$this.selector].$input[0], item);
			}
			ac[$this.selector].$ul.hide();
		};

		return list;
	};

	window.AutoComplete = AutoComplete;
})(typeof window === 'undefined' ? this : window);