Example:

	In the template:

	<input id="search-ac" type=text name="search" />

	Javascript code:


	Template.listWines.rendered = function(){
	 	var ac = AutoComplete('search-ac');
	 	ac.init({
	 		source: function(searchTerm, responseCallback){
	 			var data = ['Paris', 'New York', 'Bombay', 'Londre', 'Casablanca', 'Merzouga', 'Toulouse'];
	 			responseCallback(data);
	 		},
	 		template: function(data,i){
	 			return '<div class="box"><div><span>'+data[i]+'</span></div></div>';
	 		},
	 		clickItem: function(el, item){
	 			el.value = item.getElementsByTagName('span')[0].innerHTML;
	 		}
	 	});
 	}